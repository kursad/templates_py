import bpy

# by sambler
# taken from https://blender.stackexchange.com/questions/81115/how-to-batch-rename-bones-and-their-corresponding-vertex-groups


for rig in bpy.context.selected_objects:
    if rig.type == 'ARMATURE':
        for mesh in rig.children:
            for vg in mesh.vertex_groups:
                new_name = rig.name + '_' + vg.name
                rig.pose.bones[vg.name].name = new_name
                vg.name = new_name