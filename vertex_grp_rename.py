# creator   :   RagnarokEOTW
# url       :   https://blender.stackexchange.com/questions/69505/renaming-bones-with-python#69525

import bpy

name_list = [
        # old name - new name
        ['Bip01 Head','Bip01_Head'],
        ['Bip01 Neck1','Bip01_Neck1'],
        ## Rest of the list 
        ## Each version of the script has a different list, specific to the game engines I'm porting between.
        ['Bip01 Finger41.R', 'Bip01_R_Finger41'],
        ['Bip01 Finger42.R', 'Bip01_R_Finger42']
        ]

v_groups = bpy.context.active_object.vertex_groups
    for n in name_list:
        if n[0] in v_groups:
            v_groups[n[0]].name = n[1]
